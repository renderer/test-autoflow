var autoflow = require('autoflow');

var loadEod = function(fileName, cb) {
  console.log("Loading EOD from " + fileName);
  setTimeout(function() {
    cb(false, [1, 2, 3, 4])
  }, 1000);
};

var calculateSignal1 = function(eod, cb) {
  setTimeout(function() {
    console.log('Done calculating signal1');
    cb(false, [2, 3, 4, 5]);
  }, 1000);
};

var calculateSignal2 = function(eod, cb) {
  setTimeout(function() {
    console.log('Done calculating signal2');
    cb(false, [3, 4, 5, 6]);
  }, 5000);
};

var calculateScore1 = function(signal1) {
  console.log('Calculating score1');
  return signal1[signal1.length - 1];
};

var calculateScore2 = function(signal1, signal2) {
  console.log('Calculating score2');
  return signal1[signal1.length - 1] + signal2[0];
};

var calculateScore3 = function(signal2, cb) {
  console.log('Calculating score3');
  setTimeout(function() {
    cb(false, signal2[0]);
  }, 100);
};

var sumScore = function(score1, score2, score3) {
  return 6 * score1 + 7 * score2 + score3;
};

var showResult = function(sumScore) {
  console.log(sumScore);
}

var processData = autoflow('processData', 'fileName, cb -> err',
  loadEod, 'fileName, cb -> err, eod',
  calculateSignal1, 'eod, cb -> err, signal1',
  calculateSignal2, 'eod, cb -> err, signal2',
  calculateScore1, 'signal1 -> score1',
  calculateScore2, 'signal1, signal2 -> score2',
  calculateScore3, 'signal2, cb -> err, score3',
  sumScore, 'score1, score2, score3 -> sumScore',
  showResult, 'sumScore'
  );

processData('test.csv', function(err) {
  console.log('done');
});